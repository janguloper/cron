require("dotenv").config();
const ApolloClient = require("apollo-boost").ApolloClient;
const fetch = require("cross-fetch/polyfill").fetch;
const createHttpLink = require("apollo-link-http").createHttpLink;
const InMemoryCache = require("apollo-cache-inmemory").InMemoryCache;

const token = process.env.JWT;

const client = new ApolloClient({
  link: createHttpLink({
    uri: process.env.GRAPH_URL,
    fetch: fetch,
    headers: {
      authorization: token ? `Bearer ${token}` : "",
    },
  }),
  cache: new InMemoryCache(),
});

const clientLocal = new ApolloClient({
  link: createHttpLink({
    uri: process.env.GRAPH_URL_LOCAL,
    fetch: fetch,
    headers: {
      authorization: token ? `Bearer ${token}` : "",
    },
  }),
  cache: new InMemoryCache(),
});

const sleep = (time = 1000) => {
  return new Promise((resolve) => {
    // wait 3s before calling fn(par)
    setTimeout(() => resolve(true), time);
  });
};

module.exports = { client, sleep, clientLocal };

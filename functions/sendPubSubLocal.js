require("dotenv").config();
const gql = require("graphql-tag");
const { clientLocal, sleep } = require("./functions");

const totalPages = 1000;

module.exports = async () => {
  for (let total = 0; total <= totalPages; total = total + 1) {
    if (total > 0) {
      console.log("Espera 1/2 Minutos");
      await sleep(20000);
    }
    const query = gql`
      mutation resendContentResultPubSub(
        $startDate: String
        $endDate: String
        $limit: Int
        $page: Int
        $traking: String
      ) {
        resendContentResultPubSub(
          startDate: $startDate
          page: $page
          endDate: $endDate
          limit: $limit
          traking: $traking
        )
      }
    `;

    await clientLocal
      .mutate({
        mutation: query,
        variables: {
          startDate: "2021-10-05T00:00:00.000Z",
          endDate: "2021-10-05T23:59:59.000Z",
          page: 0,
          traking: "1",
          limit: 100,
        },
        fetchPolicy: "no-cache",
      })
      .then((res) => {
        console.log(res, total);
        return res;
      })
      .catch((e) => {
        console.log(e.message);
        return false;
      });

    if (total === totalPages) {
      return true;
    }
  }
};

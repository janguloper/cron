require("dotenv").config();
const gql = require("graphql-tag");
const { clientLocal, sleep } = require("./functions");

const totalPages = 949;

module.exports = async () => {
  for (let total = 0; total <= totalPages; total = total + 1) {
    if (total > 0) {
      console.log("Espera 1 Minutos");
      await sleep(2000);
    }
    console.log("Inicia");
    const query = gql`
      mutation createLogContentFragments(
        $startDate: String
        $endDate: String
        $page: Int
        $limit: Int
      ) {
        createLogContentFragments(
          startDate: $startDate
          endDate: $endDate
          page: $page
          limit: $limit
        )
      }
    `;

    await clientLocal
      .mutate({
        mutation: query,
        variables: {
          startDate: "2021-10-05T00:00:00.000Z",
          endDate: "2021-10-05T23:59:59.000Z",
          page: 0,
          limit: 50,
        },
        fetchPolicy: "no-cache",
      })
      .then((res) => {
        console.log(res, total);
        return res;
      })
      .catch((e) => {
        console.log(e.message);
        return false;
      });

    if (total === totalPages) {
      return true;
    }
  }
};

require("dotenv").config();
const gql = require("graphql-tag");
const { clientLocal, sleep } = require("./functions");

const totalPages = 186;

module.exports = async () => {
  for (let total = 45; total <= totalPages; total = total + 1) {
    if (total > 0) {
      console.log("Espera 1/2 Minutos");
      await sleep(10000);
    }
    console.log("Inicia");
    const query = gql`
      mutation migrateLogCoreMeterStats(
        $startDate: String
        $endDate: String
        $page: Int
        $limit: Int
      ) {
        migrateLogCoreMeterStats(
          startDate: $startDate
          endDate: $endDate
          page: $page
          limit: $limit
        )
      }
    `;

    await clientLocal
      .mutate({
        mutation: query,
        variables: {
          startDate: "2021-10-20T00:00:00.000Z",
          endDate: "2021-10-20T23:59:59.000Z",
          page: total,
          limit: 1000,
        },
        fetchPolicy: "no-cache",
      })
      .then((res) => {
        console.log(res, total);
        return res;
      })
      .catch((e) => {
        console.log(e.message);
        return false;
      });

    if (total === totalPages) {
      return true;
    }
  }
};

require("dotenv").config();
const gql = require("graphql-tag");
const { client, sleep } = require("./functions");

const totalPages = 1000;

module.exports = async () => {
  for (let total = 0; total <= totalPages; total = total + 1) {
    if (total > 0) {
      console.log("Espera 1/2 Minutos");
      await sleep(20000);
    }
    console.log("Inicia");
    const query = gql`
      mutation migrateLogCoreMeterStats(
        $startDate: String
        $endDate: String
        $page: Int
        $limit: Int
      ) {
        migrateLogCoreMeterStats(
          startDate: $startDate
          endDate: $endDate
          page: $page
          limit: $limit
        )
      }
    `;

    await client
      .mutate({
        mutation: query,
        variables: {
          startDate: "2021-10-11T00:00:00.000Z",
          endDate: "2021-10-11T23:59:59.000Z",
          page: total,
          limit: 100,
        },
        fetchPolicy: "no-cache",
      })
      .then((res) => {
        console.log(res, total);
        return res;
      })
      .catch((e) => {
        console.log(e.message);
        return false;
      });

    if (total === totalPages) {
      return true;
    }
  }
};

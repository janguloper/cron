require("dotenv").config();
const gql = require("graphql-tag");
const { client, sleep } = require("./functions");

const totalPages = 55;
module.exports = async () => {
  for (let total = 825; total <= totalPages; total = total + 1) {
    const query = gql`
      mutation createContentResults($page: Int) {
        createContentResults(page: $page)
      }
    `;

    await client
      .mutate({
        mutation: query,
        variables: {
          page: total,
        },
        fetchPolicy: "no-cache",
      })
      .then((res) => {
        console.log(res, total);
        return res;
      })
      .catch((e) => {
        console.log(e.message);
        return false;
      });

    if (total === totalPages) {
      return true;
    }
  }
};
